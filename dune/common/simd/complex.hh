#ifndef DUNE_COMMON_SIMD_COMPLEX_HH
#define DUNE_COMMON_SIMD_COMPLEX_HH

#include <dune/common/simd/simd.hh>
#include <dune/common/simd/io.hh>
#include <dune/common/math.hh>
#include <dune/common/ftraits.hh>

namespace Dune {


  /**
   * This class specifies a complex data type, where read and
   * imaginary parts are simd datatypes.
   */

  template<class S>
  class ComplexSIMD {
    S _real, _imag;

  public:
    constexpr ComplexSIMD() = default;

    constexpr ComplexSIMD(const S& re, const S& im=0.0)
      : _real(re)
      , _imag(im)
    {}

    constexpr ComplexSIMD(const ComplexSIMD&) = default;

    template<class T>
    constexpr ComplexSIMD(const ComplexSIMD<T>& other)
      : _real(other.real())
      , _imag(other.imag())
    {}

    constexpr ComplexSIMD(const std::complex<Simd::Scalar<S>>& z)
      : _real(z.real())
      , _imag(z.imag())
    {}

    constexpr ComplexSIMD(const Simd::Scalar<S>& z)
      : _real(z)
      , _imag(0.0)
    {}

    constexpr ComplexSIMD& operator=(const ComplexSIMD&) = default;

    constexpr ComplexSIMD& operator=(const S& re){
      _real = re;
      _imag = 0.0;
      return *this;
    }

    constexpr ComplexSIMD& operator=(const std::complex<Simd::Scalar<S>>& z){
      _real = z.real();
      _imag = z.imag();
      return *this;
    }

    constexpr ComplexSIMD& operator=(const Simd::Scalar<S>& z){
      _real = z;
      _imag = 0.0;
      return *this;
    }

    constexpr const S& real() const{
      return _real;
    }

    constexpr const S& imag() const{
      return _imag;
    }

    constexpr S& real(){
      return _real;
    }

    constexpr S& imag(){
      return _imag;
    }

    constexpr void real(S re){
      _real = re;
    }

    constexpr void imag(S im){
      _imag = im;
    }

    template<class T>
    constexpr ComplexSIMD& operator+=(const T& z){
      if constexpr (Impl::isComplexLike<T>::value){
        _real += z.real();
        _imag += z.imag();
      }else{
        _real += z;
      }
      return *this;
    }

    template<class T>
    constexpr ComplexSIMD& operator-=(const T& z){
      if constexpr (Impl::isComplexLike<T>::value){
        _real -= z.real();
        _imag -= z.imag();
      }else{
        _real -= z;
      }
      return *this;
    }

    template<class T>
    constexpr ComplexSIMD& operator*=(const T& z){
      if constexpr (Impl::isComplexLike<T>::value){
        auto tmp = _real*z.real() - _imag*z.imag();
        _imag = _real*z.imag() + z.real()*_imag;
        _real = tmp;
      }else {
        _real *= z;
        _imag *= z;
      }
      return *this;
    }

    template<class T>
    constexpr ComplexSIMD& operator/=(const T& z){
      if constexpr (Impl::isComplexLike<T>::value){
        auto norm = z.real()*z.real() + z.imag()*z.imag();
        auto tmp = (_real*z.real() + _imag*z.imag())/norm;
        _imag = (_imag*z.real() - _real*z.imag())/norm;
        _real = tmp;
      }else{
        _real /= z;
        _imag /= z;
      }
      return *this;
    }

  };

  using namespace Simd;
  template<class S>
  class ComplexSIMDLaneProxy
  {
  private:
    ComplexSIMD<S>& vec_;
    std::size_t idx_;

  public:

    using value_type = std::complex<Simd::Scalar<S>>;
    ComplexSIMDLaneProxy(std::size_t idx, ComplexSIMD<S>& vec)
      : vec_(vec)
      , idx_(idx)
    {}

    ComplexSIMDLaneProxy(const ComplexSIMDLaneProxy&) = delete;
    ComplexSIMDLaneProxy(ComplexSIMDLaneProxy&&) = delete;

    constexpr Scalar<S> real() const{
      return Simd::lane(idx_, vec_.real());
    }

    constexpr Scalar<S> imag() const{
      return Simd::lane(idx_, vec_.imag());
    }

    void real(Scalar<S> re){
      Simd::lane(idx_, vec_.real()) = re;
    }

    void imag(Scalar<S> im){
      Simd::lane(idx_, vec_.imag()) = im;
    }

    operator value_type() const {
      return {real(), imag()};
    }

    // assignment operators
    ComplexSIMDLaneProxy<S>& operator=(const value_type& o) &&
    {
     real(o.real());
     imag(o.imag());
     return *this;
    }

    ComplexSIMDLaneProxy<S>& operator+=(const value_type& o) &&
    {
     lane(idx_, vec_.real()) += o;
     return *this;
    }

    ComplexSIMDLaneProxy<S>& operator-=(const value_type& o) &&
    {
     lane(idx_, vec_.real()) -= o;
     return *this;
    }

    ComplexSIMDLaneProxy<S>& operator*=(const value_type& o) &&
    {
     auto tmp = real()*o.real() - imag()*o.imag();
     imag(imag()*o.real() + real()*o.imag());
     real(tmp);
     return *this;
    }

    ComplexSIMDLaneProxy<S>& operator/=(const value_type& o) &&
    {
     auto norm = o.real()*o.real() + o.imag()*o.imag();
     auto tmp = (real()*o.real() + imag()*o.imag())/norm;
     imag((imag()*o.real() + real()*o.imag())/norm);
     real(tmp);
     return *this;
    }

    friend value_type& operator+=(value_type& a, const ComplexSIMDLaneProxy& b){
      a = value_type(a.real() + b.real(), a.imag() + b.imag());
    }

    friend value_type& operator-=(value_type& a, const ComplexSIMDLaneProxy& b){
      a = value_type(a.real() - b.real(), a.imag() - b.imag());
    }

    friend value_type& operator*=(value_type& a, const ComplexSIMDLaneProxy& b){
      a = value_type(a.real() * b.real() - a.imag()*b.imag() , a.imag()*b.real() + a.real()*b.imag());
    }

    friend value_type& operator/=(value_type& a, const ComplexSIMDLaneProxy& b){
      auto norm = b.real()*b.real() + b.imag()*b.imag();
      auto tmp = (a.real()*b.real() + a.imag()*b.imag())/norm;
      a = value_type(tmp, (a.imag()*b.real() + a.real()*b.imag())/norm);
    }

    friend void swap(ComplexSIMDLaneProxy<S>&& a, ComplexSIMDLaneProxy<S>&& b){
      using std::swap;
      swap(Simd::lane(a.idx_, a.vec_.real()), Simd::lane(b.idx_, b.vec_.real()));
      swap(Simd::lane(a.idx_, a.vec_.imag()), Simd::lane(b.idx_, b.vec_.imag()));
    }

    friend void swap(value_type& a, ComplexSIMDLaneProxy<S>&& b){
      using std::swap;
      swap(a.real(), Simd::lane(b.idx_, b.real()));
      swap(a.imag(), Simd::lane(b.idx_, b.imag()));
    }

    friend void swap(ComplexSIMDLaneProxy<S>&& a, value_type& b){
      using std::swap;
      swap(Simd::lane(a.idx_, a.real()), b.real());
      swap(Simd::lane(a.idx_, a.imag()), b.imag());
    }

    template<class T>
    friend value_type operator+(const ComplexSIMDLaneProxy<S>& a, const ComplexSIMDLaneProxy<S>& b)
    {
      return {a.real()+b.real(), a.imag()+b.imag()};
    }

    template<class T>
    friend value_type operator+(const value_type& a, const ComplexSIMDLaneProxy<S>& b)
    {
      return {a.real() + b.real(),
              a.imag() + b.imag()};
    }

    template<class T>
    friend value_type operator+(const ComplexSIMDLaneProxy<S>& a, const value_type& b)
    {
      return {a.real() + b.real(),
              b.imag() + b.imag()};
    }

    template<class T>
    friend value_type operator-(const ComplexSIMDLaneProxy<S>& a, const ComplexSIMDLaneProxy<S>& b)
    {
     return {Simd::lane(a.idx_, a.vec_.real())-Simd::lane(b.idx_, b.vec_.real()),
             Simd::lane(a.idx_, a.vec_.imag())-Simd::lane(b.idx_, b.vec_.imag())};
    }

    template<class T>
    friend value_type operator-(const value_type& a, const ComplexSIMDLaneProxy<S>& b)
    {
     return {a.real()-Simd::lane(b.idx_, b.vec_.real()),
             a.imag()-Simd::lane(b.idx_, b.vec_.imag())};
    }

    template<class T>
    friend value_type operator-(const ComplexSIMDLaneProxy<S>& a, const value_type& b)
    {
     return {Simd::lane(a.idx_, a.vec_.real()) - b.real(),
             Simd::lane(a.idx_, a.vec_.imag()) - b.imag()};
    }

    template<class T>
    friend value_type operator*(const ComplexSIMDLaneProxy<S>& a, const ComplexSIMDLaneProxy<S>& b)
    {
     return {Simd::lane(a.idx_, a.vec_.real())*Simd::lane(b.idx_, b.vec_.real())
             -Simd::lane(a.idx_, a.vec_.imag())*Simd::lane(b.idx_, b.vec_.imag()),
             Simd::lane(a.idx_, a.vec_.real())*Simd::lane(b.idx_, b.vec_.imag())
             + Simd::lane(a.idx_, a.vec_.imag())*Simd::lane(b.idx_, b.vec_.real())};
    }

    template<class T>
    friend value_type operator*(const value_type& a, const ComplexSIMDLaneProxy<S>& b)
    {
     return {a.real()*Simd::lane(b.idx_, b.vec_.real())
             -a.imag()*Simd::lane(b.idx_, b.vec_.imag()),
             a.real()*Simd::lane(b.idx_, b.vec_.imag())
             + a.imag()*Simd::lane(b.idx_, b.vec_.real())};
    }

    template<class T>
    friend value_type operator*(const ComplexSIMDLaneProxy<S>& a, const value_type& b)
    {
     return {Simd::lane(a.idx_, a.vec_.real())*b.real()
             -Simd::lane(a.idx_, a.vec_.imag())*b.imag(),
             Simd::lane(a.idx_, a.vec_.real())*b.imag()
             + Simd::lane(a.idx_, a.vec_.imag())*b.real()};
    }

    template<class T>
    friend value_type operator/(const ComplexSIMDLaneProxy<S>& a, const ComplexSIMDLaneProxy<S>& b)
    {
      auto norm = Simd::lane(b.idx_, b.vec_.real())*Simd::lane(b.idx_, b.vec_.real()) + Simd::lane(b.idx_, b.vec_.imag())*Simd::lane(b.idx_, b.vec_.imag());
      return {(Simd::lane(a.idx_, a.vec_.real())*Simd::lane(b.idx_, b.vec_.real()) + Simd::lane(a.idx_, a.vec_.imag())*Simd::lane(b.idx_, b.vec_.imag()))/norm,
              (Simd::lane(a.idx_, a.vec_.real())*Simd::lane(b.idx_, b.vec_.imag()) + Simd::lane(a.idx_, a.vec_.imag())*Simd::lane(b.idx_, b.vec_.real()))/norm};
    }

    template<class T>
    friend value_type operator/(const value_type& a, const ComplexSIMDLaneProxy<S>& b)
      {
       auto norm = Simd::lane(b.idx_, b.vec_.real())*Simd::lane(b.idx_, b.vec_.real()) + Simd::lane(b.idx_, b.vec_.imag())*Simd::lane(b.idx_, b.vec_.imag());
       return {(a.real()*Simd::lane(b.idx_, b.vec_.real())
                +a.imag()*Simd::lane(b.idx_, b.vec_.imag()))/norm,
               (a.real()*Simd::lane(b.idx_, b.vec_.imag())
                - a.imag()*Simd::lane(b.idx_, b.vec_.real()))/norm};
      }

    template<class T>
    friend value_type operator/(const ComplexSIMDLaneProxy<S>& a, const value_type& b)
      {
       auto norm = b.real()*b.real() + b.imag()*b.imag();
       return {(Simd::lane(a.idx_, a.vec_.real())*b.real()
                +Simd::lane(a.idx_, a.vec_.imag())*b.imag())/norm,
               (Simd::lane(a.idx_, a.vec_.real())*b.imag()
                - Simd::lane(a.idx_, a.vec_.imag())*b.real())/norm};
      }

    friend auto abs(const ComplexSIMDLaneProxy<S>& z){
      using std::sqrt;
      return sqrt(Simd::lane(z.idx_, z.vec_.real())*Simd::lane(z.idx_, z.vec_.real()) + Simd::lane(z.idx_, z.vec_.imag())*Simd::lane(z.idx_, z.vec_.imag()));
    }
  };

  template<class S>
  constexpr ComplexSIMD<S> operator+(const ComplexSIMD<S>& z){
    return z;
  }

  template<class S>
  constexpr ComplexSIMD<S> operator-(const ComplexSIMD<S>& z){
    return {-z.real(), -z.imag()};
  }

  template<class T, class U, class = std::enable_if_t<(Impl::isComplexLike<T>::value || Impl::isComplexLike<U>::value) && !(std::is_same_v<T,Simd::Scalar<T>> && std::is_same_v<U,Simd::Scalar<U>>)>>
  constexpr auto operator+(const T& z1, const U& z2)
    -> ComplexSIMD<std::decay_t<decltype(std::declval<real_t<T>>()+std::declval<real_t<U>>())>>{
    if constexpr (Impl::isComplexLike<T>::value && Impl::isComplexLike<U>::value){
      return {z1.real()+z2.real(), z1.imag()+z2.imag()};
    }else if constexpr (Impl::isComplexLike<T>::value){
      return {z1.real()+z2, z1.imag()};
    }else{
      return {z1+z2.real(), z2.imag()};
    }
  }

  template<class T, class U, class = std::enable_if_t<(Impl::isComplexLike<T>::value || Impl::isComplexLike<U>::value) && !(std::is_same_v<T,Simd::Scalar<T>> && std::is_same_v<U,Simd::Scalar<U>>)>>
  constexpr auto operator-(const T& z1, const U& z2)
    -> ComplexSIMD<std::decay_t<decltype(std::declval<real_t<T>>()-std::declval<real_t<U>>())>>{
    if constexpr (Impl::isComplexLike<T>::value && Impl::isComplexLike<U>::value){
      return {z1.real()-z2.real(), z1.imag()-z2.imag()};
    }else if constexpr (Impl::isComplexLike<T>::value){
      return {z1.real()-z2, z1.imag()};
    }else{
      return {z1-z2.real(), z2.imag()};
    }
  }

  template<class T, class U, class = std::enable_if_t<(Impl::isComplexLike<T>::value || Impl::isComplexLike<U>::value) && !(std::is_same_v<T,Simd::Scalar<T>> && std::is_same_v<U,Simd::Scalar<U>>)>>
  constexpr auto operator*(const T& z1, const U& z2)
    -> ComplexSIMD<std::decay_t<decltype(std::declval<real_t<T>>()*std::declval<real_t<U>>())>>{
    if constexpr (Impl::isComplexLike<T>::value && Impl::isComplexLike<U>::value){
      return {z1.real()*z2.real() - z1.imag()*z2.imag(), z1.real()*z2.imag() + z1.imag()*z2.real()};
    }else if constexpr (Impl::isComplexLike<T>::value){
      return {z1.real()*z2, z1.imag()*z2};
    }else{
      return {z1*z2.real(), z1*z2.imag()};
    }
  }

  template<class T, class U, class = std::enable_if_t<(Impl::isComplexLike<T>::value || Impl::isComplexLike<U>::value) && !(std::is_same_v<T,Simd::Scalar<T>> && std::is_same_v<U,Simd::Scalar<U>>)>>
  constexpr auto operator/(const T& z1, const U& z2)
    -> ComplexSIMD<std::decay_t<decltype(std::declval<real_t<T>>()/std::declval<real_t<U>>())>>{
    if constexpr (Impl::isComplexLike<T>::value && Impl::isComplexLike<U>::value){
      auto norm = z2.real()*z2.real() + z2.imag()*z2.imag();
      return {(z1.real()*z2.real() + z1.imag()*z2.imag())/norm, (z1.real()*z2.imag() - z1.imag()*z2.real())/norm};
    }else if constexpr (Impl::isComplexLike<T>::value){
      return {z1.real()/z2, z1.imag()/z2};
    }else{
      auto norm = z2.real()*z2.real() + z2.imag()*z2.imag();
      return {z1*z2.real()/norm, z1*z2.imag()/norm};
    }
  }

  template<class S>
  constexpr bool operator==(const ComplexSIMD<S>& z1, const ComplexSIMD<S>& z2){
    return z1.real()==z2.real() && z1.imag()==z2.imag();
  }

  template<class S>
  constexpr bool operator!=(const ComplexSIMD<S>& z1, const ComplexSIMD<S>& z2){
    return !(z1==z2);
  }

  template<class S>
  constexpr bool operator==(const ComplexSIMD<S>& z1, const S& z2){
    return z1.real()==z2 && z1.imag()==S(0.0);
  }

  template<class S>
  constexpr bool operator!=(const ComplexSIMD<S>& z1, const S& z2){
    return !(z1==z2);
  }

  template<class S>
  constexpr bool operator==(const S& z1, const ComplexSIMD<S>& z2){
    return z1==z2.real() && S(0.0)==z2.imag();
  }

  template<class S>
  constexpr bool operator!=(const S& z1, const ComplexSIMD<S>& z2){
    return !(z1==z2);
  }

  template <class T, class CharT, class Traits>
  std::basic_ostream<CharT, Traits>&
  operator<<(std::basic_ostream<CharT, Traits>& os, const ComplexSIMD<T>& x){
    os << "(" << Simd::io(x.real()) << "," << Simd::io(x.imag()) << ")";
    return os;
  }

  template<class S>
  S real(const ComplexSIMD<S>& z){
    return z.real();
  }

  template<class S>
  S imag(const ComplexSIMD<S>& z){
    return z.imag();
  }

  template<class S>
  S arg(const ComplexSIMD<S>& z){
    using std::atan2;
    return atan2(z.imag(), z.real());
  }

  template<class S>
  S norm(const ComplexSIMD<S>& z){
    return z.real()*z.real() + z.imag()*z.imag();
  }

  template<class S>
  ComplexSIMD<S> conj(const ComplexSIMD<S>& z){
    return {z.real(), -z.imag()};
  }

  // implement math functions by converting to std::complex and call the corresponing function
#define DUNE_SIMD_COMPLEX_UNARY_FUN(fun)          \
  template<class S>                               \
  auto fun(const ComplexSIMD<S>& z){              \
    ComplexSIMD<S> out;                           \
    for(size_t l=0;l<Simd::lanes(z);++l){         \
      Simd::lane(l,out) = fun(Simd::lane(l, z));  \
    }                                             \
    return out;                                   \
  }                                               \
  static_assert(true, "expecting ;")

  DUNE_SIMD_COMPLEX_UNARY_FUN(sin);
  DUNE_SIMD_COMPLEX_UNARY_FUN(cos);
  DUNE_SIMD_COMPLEX_UNARY_FUN(tan);
  DUNE_SIMD_COMPLEX_UNARY_FUN(asin);
  DUNE_SIMD_COMPLEX_UNARY_FUN(acos);
  DUNE_SIMD_COMPLEX_UNARY_FUN(atan);
  DUNE_SIMD_COMPLEX_UNARY_FUN(sinh);
  DUNE_SIMD_COMPLEX_UNARY_FUN(cosh);
  DUNE_SIMD_COMPLEX_UNARY_FUN(tanh);
  DUNE_SIMD_COMPLEX_UNARY_FUN(asinh);
  DUNE_SIMD_COMPLEX_UNARY_FUN(acosh);
  DUNE_SIMD_COMPLEX_UNARY_FUN(atanh);

  DUNE_SIMD_COMPLEX_UNARY_FUN(exp);
  DUNE_SIMD_COMPLEX_UNARY_FUN(log);
  DUNE_SIMD_COMPLEX_UNARY_FUN(log10);

  DUNE_SIMD_COMPLEX_UNARY_FUN(sqrt);
  DUNE_SIMD_COMPLEX_UNARY_FUN(cqrt);

#undef DUNE_SIMD_COMPLEX_UNARY_FUN

  template<class S>
  auto abs(const ComplexSIMD<S>& z){
    S out;
    for(size_t l=0;l<Simd::lanes(z);++l){
      Simd::lane(l,out) = abs(Simd::lane(l, z));
    }
    return out;
  }

  namespace Simd {
    namespace Overloads {
      template<class S>
      struct ScalarType<ComplexSIMD<S>> {
        using type = std::complex<Scalar<S>>;
      };

      template<class U, class S>
      struct RebindType<std::complex<U>, ComplexSIMD<S>> {
        using type = ComplexSIMD<Rebind<U,S>>;
      };

      template<class U, class S>
      struct RebindType<U, ComplexSIMD<S>> {
        using type = Rebind<U,S>;
      };

      //Implementation of SIMD-interface-functionality
      template<class S>
      struct LaneCount<ComplexSIMD<S>> : index_constant<Simd::lanes<S>()> {};

      template<class S>
      auto cond(ADLTag<5>, Simd::Mask<ComplexSIMD<S>> mask,
                ComplexSIMD<S> ifTrue, ComplexSIMD<S> ifFalse) {
        ComplexSIMD<S> out;
        out.real(Simd::cond(mask, ifTrue.real(), ifFalse.real()));
        out.imag(Simd::cond(mask, ifTrue.imag(), ifFalse.imag()));
        return out;
      }

      template<class S>
      std::complex<Scalar<S>> lane(ADLTag<5>, std::size_t l, ComplexSIMD<S>&& z){
        return {Simd::lane(l, z.real()), Simd::lane(l, z.imag())};
      }

      template<class S>
      std::complex<Scalar<S>> lane(ADLTag<5>, std::size_t l, const ComplexSIMD<S>& z){
        return {Simd::lane(l, z.real()), Simd::lane(l, z.imag())};
      }

      template<class S>
      ComplexSIMDLaneProxy<S> lane(ADLTag<5>, std::size_t l, ComplexSIMD<S>& z){
        return ComplexSIMDLaneProxy<S>(l, z);
      }
    }
  }

  template<class S>
  struct IsNumber<ComplexSIMD<S>> :
    public IsNumber<S> {};

  template<class S>
  struct FieldTraits< ComplexSIMD<S> >
  {
    typedef ComplexSIMD<S> field_type;
    typedef S real_type;
  };
}

#endif
