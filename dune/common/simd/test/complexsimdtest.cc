#include <config.h>

#include <iostream>

#include <dune/common/simd/complex.hh>
#include <dune/common/simd/loop.hh>

using S = Dune::LoopSIMD<double, 4>;

int main(int argc, char** argv){

  using namespace std::complex_literals;
  Dune::ComplexSIMD<S> z(42.0 + 3i);
  //Dune::Simd::lane(2, z) *= 5.0;
  Dune::Simd::lane(2, z) /= abs(Dune::Simd::lane(3, z));
  std::complex<double> zs = Dune::Simd::lane(0, z);
  std::cout << z*z +z << zs  << sqrt(z)<< std::endl;
  return 0;
}
